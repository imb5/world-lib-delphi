unit WorldDataImage;

interface

uses
  // vcl
  Vcl.graphics,
  Vcl.Imaging.pngimage,
  // todo: fmx
  //..
  IdCoderMIME, // base64
  System.Classes,
  System.SysUtils;

// images and base64
function ImageToBytes(aImage: TPngImage): TBytes;
function GraphicToBytes(aGraphic: TGraphic): TBytes;
procedure BytesToImage(const aBytes: TBytes; aImage: TPngImage);
function ImageToBase64(aImage: TPngImage): string;
function PNGFileToBase64(const aFileName: string): string;


implementation

{ utils }

function ImageToBytes(aImage: TPngImage): TBytes;
var
  stream: TBytesStream;
begin
  stream := TBytesStream.Create;
  try
    aImage.SaveToStream(stream);
  	Result := stream.Bytes;
  finally
    stream.Free;
  end;
end;

function GraphicToBytes(aGraphic: TGraphic): TBytes;
var
  stream: TBytesStream;
begin
  stream := TBytesStream.Create;
  try
    aGraphic.SaveToStream(stream);
  	Result := stream.Bytes;
  finally
    stream.Free;
  end;
end;

{
function CreateImageFromBytes(const aBytes: TBytes): TPngImage;
var
  stream: TBytesStream;
begin
  stream := TBytesStream.Create(aBytes);
  try
    Result := TPngImage.Create;
    Result.LoadFromStream(stream);
  finally
    stream.Free;
  end;
end;
}
procedure BytesToImage(const aBytes: TBytes; aImage: TPngImage);
var
  stream: TBytesStream;
begin
  stream := TBytesStream.Create(aBytes);
  try
    aImage.LoadFromStream(stream);
  finally
    stream.Free;
  end;
end;

function ImageToBase64(aImage: TPngImage): string;
var
  ms: TMemoryStream;
begin
  if Assigned(aImage) then
  begin
    ms := TMemoryStream.Create;
    try
      aImage.SaveToStream(ms);
      ms.Position := 0;
      Result := 'data:image/png;base64,'+TIdEncoderMIME.EncodeStream(ms);
    finally
      ms.Free;
    end;
  end
  else Result := '';
end;

function PNGFileToBase64(const aFileName: string): string;
var
  fs: TFileStream;
begin
  fs := TFileStream.Create(aFileName, fmOpenRead);
  try
    fs.Position := 0;
    Result := 'data:image/png;base64,'+TIdEncoderMIME.EncodeStream(fs);
  finally
    fs.Free;
  end;
end;



end.
