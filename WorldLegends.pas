unit WorldLegends;

interface

uses
  WorldDataCode,
  imb4,
  System.JSON,
  System.SysUtils, System.Math;

type
  // [minValue, maxValue>
  TDiscretePaletteEntry = record
  class function Create(aColors: TGeoColors; aMinValue, aMaxValue: Double; const aDescription: string): TDiscretePaletteEntry; static;
  public
    colors: TGeoColors;
    minValue: Double;
    maxValue: Double;
    description: string;
    function ValueInRange(const aValue: Double): Boolean;
    function Encode: TWDValue;
    procedure Decode(const aBuffer: TWDValue; var aCursor: Integer; aLimit: Integer);
  end;

  TPaletteDiscreteEntryArray = array of TDiscretePaletteEntry;

  TDiscretePalette = class(TWDPalette)
  constructor Create(const aDescription: string; const aEntries: TPaletteDiscreteEntryArray; aNoDataColor: TGeoColors); overload;
  constructor CreateFromJSON(const aJSON: string);
  destructor Destroy; override;
  //class function WorldType: TWDWorldType; override;
  class function wdTag: UInt32; override;
  public
    function Encode: TWDValue; override;
    function Decode(const aBuffer: TWDValue; var aCursor: Integer; aLimit: Integer): TWDClass; override;
  private
    fEntries: TPaletteDiscreteEntryArray;
    fNoDataColors: TGeoColors;
  protected
    function _clone: TWDPalette; override;
  public
    property entries: TPaletteDiscreteEntryArray read fEntries;
    property noDataColors: TGeoColors read fNoDataColors;
    function ValueToColors(const aValue: Double): TGeoColors; override;
    function minValue: Double; override;
    function maxValue: Double; override;
  end;

  TRampPaletteEntry = record
  class function Create(aColor: TAlphaRGBPixel; aValue: Double; const aDescription: string): TRampPaletteEntry; static;
  public
    color: TAlphaRGBPixel;
    value: Double;
    description: string;
    function Encode: TWDValue;
    procedure Decode(const aBuffer: TWDValue; var aCursor: Integer; aLimit: Integer);
  end;

  TPaletteRampEntryArray = array of TRampPaletteEntry;

  // values in entries: low to high
  TRampPalette = class(TWDPalette)
  constructor Create(const aDescription: string; const aEntries: TPaletteRampEntryArray; aLowerDataColor, aNoDataColor, aHigherDataColor: TAlphaRGBPixel); overload;
  constructor CreateFromJSON(const aJSON: string);
  destructor Destroy; override;
  //class function WorldType: TWDWorldType; override;
  class function wdTag: UInt32; override;
  public
    function Encode: TWDValue; override;
    function Decode(const aBuffer: TWDValue; var aCursor: Integer; aLimit: Integer): TWDClass; override;
  private
    fEntries: TPaletteRampEntryArray;
    fLowerDataColor: TAlphaRGBPixel;
    fNoDataColor: TAlphaRGBPixel;
    fHigherDataColor: TAlphaRGBPixel;
  protected
    function _clone: TWDPalette; override;
  public
    property entries: TPaletteRampEntryArray read fEntries;
    function ValueToColors(const aValue: Double): TGeoColors; override;
    function minValue: Double; override;
    function maxValue: Double; override;
    property lowerDataColor: TAlphaRGBPixel read fLowerDataColor;
    property noDataColor: TAlphaRGBPixel read fNoDataColor;
    property higherDataColor: TAlphaRGBPixel read fHigherDataColor;
  end;

  {
  TDuoRampPalette = class(TWDPalette)

  end;
  }

function FormatLegendDescription(const aDescription: string): string;

implementation

function RampByte(aFraction: Double; aLow, aHigh: Byte): Cardinal;
begin
  Result := Round(aLow+aFraction*(aHigh-aLow));
end;

function ColorRamp(aValue, aLowValue, aHighValue: Double; aLowColor, aHighColor: TAlphaRGBPixel): TAlphaRGBPixel;
var
  f: Double;
begin
  f := (aValue-aLowValue)/(aHighValue-aLowValue);
  Result :=
    (RampByte(f, (aLowColor shr  0) and $FF, (aHighColor shr  0) and $FF) shl  0) or
    (RampByte(f, (aLowColor shr  8) and $FF, (aHighColor shr  8) and $FF) shl  8) or
    (RampByte(f, (aLowColor shr 16) and $FF, (aHighColor shr 16) and $FF) shl 16) or
    (RampByte(f, (aLowColor shr 24) and $FF, (aHighColor shr 24) and $FF) shl 24);
end;

function FormatLegendDescription(const aDescription: string): string;
var
  p: integer;
  i: Integer;
  s: string;
  v: Integer;
begin
  Result := aDescription;
  p := Pos('~~', Result);
  while p<>0 do
  begin
    if (p>1) then
    begin
      Delete(Result, p, 2);
      if Result[p-1]<>'-'
      then Insert(' ', Result, p);
    end
    else
    begin
      Delete(Result, p, 2);
      Insert(' ', Result, p);
    end;
    p := Pos('~~', Result);
  end;
  i := 1;
  while i<Length(Result) do
  begin
    if (Result[i]='\') and (Result[i+1]<>'\') then
    begin
      s := Copy(Result, i+1, 3);
      //v := (Ord(s[1])-Ord('0'))*8*8+(Ord(s[2])-Ord('0'))*8+(Ord(s[3])-Ord('0'));
      v := StrToIntDef(s, 32);
      Delete(Result, i, 4);
      Insert(Chr(v), Result, i);
    end;
    i := i+1;
  end;
end;

{ TDiscretePaletteEntry }

class function TDiscretePaletteEntry.Create(aColors: TGeoColors; aMinValue, aMaxValue: Double;
  const aDescription: string): TDiscretePaletteEntry;
begin
  Result.colors := aColors;
  Result.minValue := aMinValue;
  Result.maxValue := aMaxValue;
  Result.description := aDescription;
end;

procedure TDiscretePaletteEntry.Decode(const aBuffer: TWDValue; var aCursor: Integer; aLimit: Integer);
begin
  colors.outlineColor := aBuffer.bb_read_uint32(aCursor);
  colors.fillColor := aBuffer.bb_read_uint32(aCursor);
  colors.fill2Color := aBuffer.bb_read_uint32(aCursor);
  minValue := aBuffer.bb_read_double(aCursor);
  maxValue := aBuffer.bb_read_double(aCursor);
  description := aBuffer.bb_read_string(aCursor);
  if aCursor>aLimit
  then Exception.Create('TPaletteDiscreteEntry.Decode read over limit ('+aCursor.toString+', '+aLimit.toString);
end;

function TDiscretePaletteEntry.Encode: TWDValue;
begin
  Result :=
    TByteBuffer.bb_uint32(colors.outlineColor)+
    TByteBuffer.bb_uint32(colors.fillColor)+
    TByteBuffer.bb_uint32(colors.fill2Color)+
    TByteBuffer.bb_bytes(minValue, SizeOf(minValue))+
    TByteBuffer.bb_bytes(maxValue, SizeOf(maxValue))+
    TByteBuffer.bb_string(description);
end;

function TDiscretePaletteEntry.ValueInRange(const aValue: Double): Boolean;
begin
  Result := (IsNaN(MinValue) or (MinValue <= aValue)) and (IsNaN(MaxValue) or (aValue < MaxValue));
end;

{ TDiscretePalette }

constructor TDiscretePalette.Create(const aDescription: string; const aEntries: TPaletteDiscreteEntryArray; aNoDataColor: TGeoColors);
begin
  inherited Create(aDescription);
  fEntries := aEntries;
  fNoDataColors := aNoDataColor;
end;

constructor TDiscretePalette.CreateFromJSON(const aJSON: string);
var
  jsonObject: TJSONObject;
  jsonEntries: TJSONArray;
  e: TJSONValue;
  i: Integer;
  geoColor: TJSONValue;
begin
  jsonObject := TJSONObject.ParseJSONValue(aJSON) as TJSONObject;
  inherited Create(jsonObject.getValue<string>('description'));
  // decode entries
  jsonEntries := jsonObject.getValue<TJSONArray>('entries');
  setLength(fEntries, jsonEntries.Count);
  i := 0;
  for e in jsonEntries do
  begin
    geoColor := e.GetValue<TJSONValue>('colors');
    fEntries[i] := TDiscretePaletteEntry.Create(
      TGeoColors.Create(geoColor.GetValue<Cardinal>('fill'), geoColor.GetValue<Cardinal>('outline'), geoColor.GetValue<Cardinal>('fill2')),
      e.GetValue<double>('minvalue'),
      e.GetValue<double>('maxvalue'),
      e.GetValue<string>('description'));
    i := i+1;
  end;
  // data exception colors
  geoColor := jsonObject.GetValue<TJSONValue>('nodatacolors');
  fNoDataColors := TGeoColors.Create(geoColor.GetValue<Cardinal>('fill'), geoColor.GetValue<Cardinal>('outline'), geoColor.GetValue<Cardinal>('fill2'));
end;

function TDiscretePalette.Decode(const aBuffer: TWDValue; var aCursor: Integer; aLimit: Integer): TWDClass;
begin
  inherited Decode(aBuffer, aCursor, aLimit);
  fNoDataColors.fillColor := aBuffer.bb_read_uint32(aCursor);
  fNoDataColors.outlineColor := aBuffer.bb_read_uint32(aCursor);
  fNoDataColors.fill2Color := aBuffer.bb_read_uint32(aCursor);
  while aCursor<aLimit do
  begin
    setLength(fEntries, length(fEntries)+1);
    fEntries[length(fEntries)-1].Decode(aBuffer, aCursor, aLimit);
  end;
  Result := Self;
end;

destructor TDiscretePalette.Destroy;
begin
  setLength(fEntries, 0);
  inherited;
end;

function TDiscretePalette.Encode: TWDValue;
var
  i: Integer;
begin
  Result := inherited Encode;
  Result := Result+
    TByteBuffer.bb_uint32(fNoDataColors.fillColor)+
    TByteBuffer.bb_uint32(fNoDataColors.outlineColor)+
    TByteBuffer.bb_uint32(fNoDataColors.fill2Color);
  for i := 0 to length(fEntries)-1
  do Result := Result+fEntries[i].Encode;
end;

function TDiscretePalette.maxValue: Double;
var
  i: Integer;
begin
  Result := NaN;
  for i := 0 to length(fEntries)-1 do
  begin
    if IsNaN(Result)
    then Result := fEntries[i].maxValue
    else Result := Max(Result, fEntries[i].maxValue);
  end;
end;

function TDiscretePalette.minValue: Double;
var
  i: Integer;
begin
  Result := NaN;
  for i := 0 to length(fEntries)-1 do
  begin
    if IsNaN(Result)
    then Result := fEntries[i].minValue
    else Result := Min(Result, fEntries[i].minValue);
  end;
end;

function TDiscretePalette.ValueToColors(const aValue: Double): TGeoColors;
var
  e: Integer;
begin
  if not IsNaN(aValue) then
  begin
    e := Length(fEntries)-1;
    while (e>=0) and not fEntries[e].ValueInRange(aValue)
    do e := e-1;
    if e>=0
    then Result := fEntries[e].colors
    else Result := fNoDataColors;
  end
  else Result := fNoDataColors;
end;

class function TDiscretePalette.wdTag: UInt32;
begin
  Result := icehDiscretePalette;
end;
{
class function TDiscretePalette.WorldType: TWDWorldType;
begin
  Result := wdkPaletteDiscrete;
end;
}
function TDiscretePalette._clone: TWDPalette;
begin
  Result := TDiscretePalette.Create(fDescription);
  (Result as  TDiscretePalette).fEntries := fEntries;
  (Result as  TDiscretePalette).fNoDataColors := fNoDataColors;
end;

{ TRampPaletteEntry }

class function TRampPaletteEntry.Create(aColor: TAlphaRGBPixel; aValue: Double; const aDescription: string): TRampPaletteEntry;
begin
  Result.color := aColor;
  Result.value := aValue;
  Result.description := aDescription;
end;

procedure TRampPaletteEntry.Decode(const aBuffer: TWDValue; var aCursor: Integer; aLimit: Integer);
begin
  color := aBuffer.bb_read_uint32(aCursor);
  value := aBuffer.bb_read_double(aCursor);
  description := aBuffer.bb_read_string(aCursor);
  if aCursor>aLimit
  then Exception.Create('TPaletteRampEntry.Decode read over limit ('+aCursor.toString+', '+aLimit.toString);
end;

function TRampPaletteEntry.Encode: TWDValue;
begin
  Result := Result+
    TByteBuffer.bb_uint32(color)+
    TByteBuffer.bb_bytes(value, SizeOf(value))+
    TByteBuffer.bb_string(description);
end;

{ TRampPalette }

constructor TRampPalette.Create(const aDescription: string; const aEntries: TPaletteRampEntryArray; aLowerDataColor, aNoDataColor, aHigherDataColor: TAlphaRGBPixel);
begin
  inherited Create(aDescription);
  fEntries := aEntries;
  fLowerDataColor := aLowerDataColor;
  fNoDataColor := aNoDataColor;
  fHigherDataColor := aHigherDataColor;
end;

constructor TRampPalette.CreateFromJSON(const aJSON: string);
var
  jsonObject: TJSONObject;
  jsonEntries: TJSONArray;
  e: TJSONValue;
  i: Integer;
begin
  jsonObject := TJSONObject.ParseJSONValue(aJSON) as TJSONObject;
  inherited Create(jsonObject.getValue<string>('description'));
  // decode entries
  jsonEntries := jsonObject.getValue<TJSONArray>('entries');
  setLength(fEntries, jsonEntries.Count);
  i := 0;
  for e in jsonEntries do
  begin
    fEntries[i] := TRampPaletteEntry.Create(
      e.GetValue<cardinal>('color'),
      e.GetValue<double>('value'),
      e.GetValue<string>('description'));
    i := i+1;
  end;
  // data exception colors
  fLowerDataColor := jsonObject.GetValue<cardinal>('lowerdatacolor');
  fNoDataColor := jsonObject.GetValue<cardinal>('nodatacolor');
  fHigherDataColor := jsonObject.GetValue<cardinal>('higherdatacolor');
end;

function TRampPalette.Decode(const aBuffer: TWDValue; var aCursor: Integer; aLimit: Integer): TWDClass;
begin
  inherited Decode(aBuffer, aCursor, aLimit);
  fLowerDataColor := aBuffer.bb_read_uint32(aCursor);
  fNoDataColor := aBuffer.bb_read_uint32(aCursor);
  fHigherDataColor := aBuffer.bb_read_uint32(aCursor);
  while aCursor<aLimit do
  begin
    setLength(fEntries, length(fEntries)+1);
    fEntries[length(fEntries)-1].Decode(aBuffer, aCursor, aLimit);
  end;
  Result := Self;
end;

destructor TRampPalette.Destroy;
begin
  setLength(fEntries, 0);
  inherited;
end;

function TRampPalette.Encode: TWDValue;
var
  i: Integer;
begin
  Result := inherited Encode;
  Result := Result+
    TByteBuffer.bb_uint32(fLowerDataColor)+
    TByteBuffer.bb_uint32(fNoDataColor)+
    TByteBuffer.bb_uint32(fHigherDataColor);
  for i := 0 to length(fEntries)-1
  do Result := Result+fEntries[i].Encode;
end;

function TRampPalette.maxValue: Double;
var
  i: Integer;
begin
  Result := NaN;
  for i := 0 to length(fEntries)-1 do
  begin
    if IsNaN(Result)
    then Result := fEntries[i].value
    else Result := Max(Result, fEntries[i].value);
  end;
end;

function TRampPalette.minValue: Double;
var
  i: Integer;
begin
  Result := NaN;
  for i := 0 to length(fEntries)-1 do
  begin
    if IsNaN(Result)
    then Result := fEntries[i].value
    else Result := Min(Result, fEntries[i].value);
  end;
end;

function TRampPalette.ValueToColors(const aValue: Double): TGeoColors;
var
  e: Integer;
begin
  Result := TGeoColors.Create(fNoDataColor);
  if (Length(fEntries)>0) and not IsNaN(aValue) then
  begin
    e := Length(fEntries)-1;
    // check upper limit
    if aValue<=fEntries[e].value then
    begin
      // e always less then length
      while (e>=0) and (aValue<fEntries[e].value)
      do e := e-1;
      // check within limits
      if e>=0 then
      begin
        // check for NOT last entry
        if e<Length(fEntries)-1 then
        begin
          if not SameValue(aValue, fEntries[e].Value)
          then Result.fillColor := ColorRamp(aValue, fEntries[e].Value, fEntries[e+1].Value, fEntries[e].Color, fEntries[e+1].Color)
          else Result.fillColor := fEntries[e].color;
        end
        else Result.fillColor := fEntries[e].color;
      end
      else Result.fillColor := fLowerDataColor;
    end
    else Result.fillColor := fHigherDataColor;
  end;
end;

class function TRampPalette.wdTag: UInt32;
begin
  Result := icehRampPalette;
end;
{
class function TRampPalette.WorldType: TWDWorldType;
begin
  Result := wdkPaletteRamp;
end;
}
function TRampPalette._clone: TWDPalette;
begin
  Result := TRampPalette.Create(fDescription);
  (Result as TRampPalette).fEntries := fEntries;
  (Result as TRampPalette).fLowerDataColor := fLowerDataColor;
  (Result as TRampPalette).fNoDataColor := fNoDataColor;
  (Result as TRampPalette).fHigherDataColor := fHigherDataColor;
end;

end.
